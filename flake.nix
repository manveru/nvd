{
  description = "nvd";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    ...
  }: (flake-utils.lib.eachDefaultSystem (
    system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      apps.default = {
        type = "app";
        program = "${self.packages.${system}.nvd}/bin/nvd";
      };
      packages = {
        default = self.packages.${system}.nvd;
        nvd = pkgs.callPackage ./default.nix {};
      };
    }
  ));
}
